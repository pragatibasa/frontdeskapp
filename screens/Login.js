import React, {Component} from 'react';
import {TextInput, StyleSheet, View, Text, Button, Alert} from 'react-native';
import {connect} from 'react-redux';
import {loginUser} from '../redux/actions/Auth';

class Login extends Component {
  constructor() {
    super();
    this.state = {
      email: '',
      password: '',
      errors: {},
    };
    this.authenticate = this.authenticate.bind(this);
  }

  authenticate = () => {
    const userData = {
      email: this.state.email,
      password: this.state.password,
    };
    this.props.loginUser(userData, this.props.navigation);
  };

  render() {
    return (
      <View style={styles.container}>
        <Text>Log In</Text>
        <TextInput
          style={{height: 50}}
          placeholder="Email address"
          onChangeText={text => this.setState({email: text})}
        />
        <TextInput
          secureTextEntry={true}
          style={{height: 50}}
          placeholder="Password"
          onChangeText={text => this.setState({password: text})}
        />
        <Button title="Log In" onPress={this.authenticate.bind(this)} />
        <Text style={{paddingTop: 50}}>Don't have an account? Sign Up</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
});

const mapStateToProps = state => ({
  token: state.token,
});

const mapDispatchToProps = dispatch => ({
  loginUser: (userData, navigation) =>
    dispatch(loginUser(userData, navigation)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Login);
