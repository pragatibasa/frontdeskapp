import React, {Component} from 'react';
import {connect} from 'react-redux';
import {ActivityIndicator, StatusBar, StyleSheet, View} from 'react-native';
import {getUserToken} from '../redux/actions/Auth';

class RouterScreen extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props
      .getUserToken()
      .then(() => {
        this.props.navigation.navigate(
          this.props.token.token !== null ? 'Auth' : 'Auth',
        );
      })
      .catch(error => {
        this.setState({error});
      });
  }

  render() {
    return (
      <View style={styles.container}>
        <ActivityIndicator />
        <StatusBar barStyle="default" />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

const mapStateToProps = state => ({
  token: state.token,
});

const mapDispatchToProps = dispatch => ({
  getUserToken: () => dispatch(getUserToken()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(RouterScreen);
