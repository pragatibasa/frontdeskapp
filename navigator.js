import React from 'react';
import {createSwitchNavigator, createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

import RouterScreen from './screens/RouterScreen';
import Login from './screens/Login';
import HomeScreen from './screens/HomeScreen';

export const AppStack = createStackNavigator({Home: HomeScreen});
export const AuthStack = createStackNavigator({auth: Login});

const AppNavigatorStack = createSwitchNavigator(
  {
    AuthLoading: RouterScreen,
    App: AppStack,
    Auth: AuthStack,
  },
  {
    initialRouteName: 'AuthLoading',
  },
);

const AppNavigator = createAppContainer(AppNavigatorStack);

export default AppNavigator;
