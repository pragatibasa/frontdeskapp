import AsyncStorage from '@react-native-community/async-storage';
import {Alert} from 'react-native';
import axios from '../../axios';

export const getToken = token => ({
  type: 'GET_TOKEN',
  token,
});

export const saveToken = token => ({
  type: 'SAVE_TOKEN',
  token,
});

export const removeToken = () => ({
  type: 'REMOVE_TOKEN',
});

export const loading = bool => ({
  type: 'LOADING',
  isLoading: bool,
});

export const error = error => ({
  type: 'ERROR',
  error,
});

export const getUserToken = () => dispatch =>
  AsyncStorage.getItem('userToken')
    .then(data => {
      dispatch(loading(false));
      dispatch(getToken(data));
    })
    .catch(err => {
      dispatch(loading(false));
      dispatch(error(err.message || 'ERROR'));
    });

export const saveUserToken = data => dispatch =>
  AsyncStorage.setItem('userToken', data)
    .then(data => {
      dispatch(loading(false));
      dispatch(saveToken('token saved'));
    })
    .catch(err => {
      dispatch(loading(false));
      dispatch(error(err.message || 'ERROR'));
    });

export const removeUserToken = () => dispatch =>
  AsyncStorage.removeItem('userToken')
    .then(data => {
      dispatch(loading(false));
      dispatch(removeToken(data));
    })
    .catch(err => {
      dispatch(loading(false));
      dispatch(error(err.message || 'ERROR'));
    });

export const loginUser = (userData, navigation) => dispatch => {
  return axios
    .post('/api/user/login', userData)
    .then(res => {
      const {token} = res.data;
      // Set token to Auth header
      dispatch(saveUserToken(token));
      navigation.navigate('App');
    })
    .catch(errorResult => {
      Alert.alert('Please check your credentials');
      dispatch(error(errorResult.message || 'ERROR'));
    });
};
