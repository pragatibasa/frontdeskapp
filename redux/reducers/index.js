import {combineReducers} from 'redux';
import rootReducer from './AuthToken';

export default combineReducers({
  token: rootReducer,
});
