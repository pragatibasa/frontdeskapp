const rootReducer = (
  state = {
    token: {},
    loading: true,
    error: null,
  },
  action,
) => {
  switch (action.type) {
    case 'GET_TOKEN':
      return {...state, token: action.token};
    case 'SAVE_TOKEN':
      return {...state, token: action.token};
    case 'REMOVE_TOKEN':
      return {...state, token: action.token};
    case 'LOADING':
      return {...state, loading: action.isLoading};
    case 'ERROR':
      return {...state, error: action.error};
    case 'LOGIN_REQUEST':
      return {
        ...state,
        loginSuccess: null,
        loginFailure: null,
        loginLoading: true,
        loginRequested: Date.now(),
      };
    case 'LOGIN_SUCCESS':
      return {
        ...state,
        isAuthenticated: !(action.payload),
        loginSuccess: action.payload,
        loginFailure: null,
        loginLoading: false,
      };
    case 'LOGIN_FAILURE':
      return {
        ...state,
        loginSuccess: null,
        loginFailure: action.payload,
        loginLoading: false,
      };
    default:
      return state;
  }
};

export default rootReducer;
